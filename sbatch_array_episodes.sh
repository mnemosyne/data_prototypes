#!/usr/bin/env bash
# Inform to sbatch the number of classes separated by commas either here or with --array=10,20
##SBATCH --array=10,20,30
#Job name
#SBATCH -Jprototype
# Asking for one node
#SBATCH -N1
#SBATCH --ntasks 24

NJOBS=$SLURM_NTASKS

python3 prototype_main.py --exp episodes --n_episodes 100 --n_jobs $NJOBS --n_classes $SLURM_ARRAY_TASK_ID



