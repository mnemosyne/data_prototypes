#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 24 15:27:52 2017

@author: thalita

Extracting inception v3 features for omniglot, mnist and cifar10
It is a bit of an overkill since:
1) It is an RGB processing net
2) It expects a minimum image size of 139x139 pixels.
This yields features dimension of 2048.
The default size is 299x299

To comply with the input format I repeat the gray-levels
image on the 3 channels and do a 2D interpolation
to augment it.

With 139 size, running on my 4GB GPU I could process 1/5 of
the ~33K images at a time and the total runtime
is about 5 minutes (if i let the GPU alone).
"""

from keras.applications.inception_v3 import (InceptionV3,
                                             preprocess_input)
import numpy as np
from tqdm import tqdm
import tensorflow as tf
import keras.backend as K
from keras.datasets import cifar10, mnist

DATA = "./Data/omniglot.npy"


def inceptionV3_feature(img_batch):
    resize = tf.image.resize_bilinear(
        tf.convert_to_tensor(img_batch), (139, 139))
    with tf.Session() as sess:
        img_batch = sess.run(resize)
    _, h, w, ch = img_batch.shape
    model = InceptionV3(weights='imagenet', input_shape=(h,w,ch),
                        include_top=False)
    img_batch = preprocess_input(img_batch)
    features = model.predict(img_batch)
    return features

def omniglot_extract_features():
    tf.logging.set_verbosity(tf.logging.INFO)

    images = np.load(DATA)
    n_classes, n_samples_per_class, height, width = images.shape
    images = np.reshape(images,
                        (n_classes*n_samples_per_class, 28, 28, 1))
    images = np.repeat(images, 3, axis=-1)

    batch_size = bs = images.shape[0]//5
    features = [inceptionV3_feature(images[i:i+batch_size, :, :, :])
                for i in tqdm(range(0, images.shape[0], bs),
                              desc="feature xtract")]
    features = np.concatenate(features)
    np.save("./Data/omniglot_inceptionV3_feature_maps.npy", features)
    features = np.mean(features, axis=(1, 2))
    features = np.reshape(features, (n_classes, n_samples_per_class, -1))
    np.save("./Data/omniglot_inceptionV3_features.npy", features)


def mnist_extract_features():
    tf.logging.set_verbosity(tf.logging.INFO)

    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    images = np.concatenate((x_train, x_test), axis=0)


    if K.image_data_format() == 'channels_first':
        images = images.reshape((images.shape[0], 1, images.shape[1], images.shape[2]))
        images = np.repeat(images, 3, axis=1)
    else:  # channels last
        images = images.reshape((images.shape[0], images.shape[1], images.shape[2], 1))
        images = np.repeat(images, 3, axis=-1)

    batch_size = bs = images.shape[0]//5
    features = [inceptionV3_feature(images[i:i+batch_size, ...])
                for i in tqdm(range(0, images.shape[0], bs),
                              desc="feature xtract")]
    features = np.concatenate(features, axis=0)
    np.save("./Data/mnist_inceptionV3_feature_maps.npy", features)
    if K.image_data_format() == 'channels_first':
        features = np.mean(features, axis=(3, 4))
    else:  # channels last
        features = np.mean(features, axis=(1, 2))

    np.save("./Data/mnist_inceptionV3_features.npy", features)

def cifar10_extract_features():
    tf.logging.set_verbosity(tf.logging.INFO)

    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    images = np.concatenate([x_train,x_test])

    batch_size = bs = images.shape[0]//15
    features = [inceptionV3_feature(images[i:i+batch_size, :, :, :])
                for i in tqdm(range(0, images.shape[0], bs),
                              desc="feature xtract")]
    features = np.concatenate(features)
    np.save("./Data/cifar10_inceptionV3_feature_maps.npy", features)
    features = np.mean(features, axis=(1, 2))
    np.save("./Data/cifar10_inceptionV3_features.npy", features)

if __name__ == "__main__":
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument('dataset')
    args = p.parse_args
    if args.dataset == 'omniglot':
        omniglot_extract_features()
    elif args.dataset == 'mnist':
        mnist_extract_features()
    elif args.dataset == 'cifar10':
        cifar10_extract_features()