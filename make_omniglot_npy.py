#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun  3 15:01:01 2018

@author: thalita

Adapted from markdtw's repo:
    https://github.com/markdtw/matching-networks/blob/master/utils.py

Omniglot dataset to be downloaded from:
    https://github.com/brendenlake/omniglot/tree/master/python

Modify DATA_DIR to match your omniglot location. It should contain
images_background and images_evaluation folders.

"""

import numpy as np
import os
from tqdm import tqdm
import scipy
import scipy.misc
import glob


DATA_DIR = './Data/omniglot/'


def read_omniglot():
    """Read omniglot dataset, save them to a single npy file"""
    omniglot_train = os.path.join(DATA_DIR, 'images_background')
    omniglot_eval = os.path.join(DATA_DIR, 'images_evaluation')

    data = []
    for r in [omniglot_train, omniglot_eval]:
        classes = glob.glob(r + '/*')
        for cls in tqdm(classes):
            alphabets = glob.glob(cls + '/*')
            for a in alphabets:
                characters = glob.glob(a + '/*')
                raws = []
                for ch in characters:  # 20 iters
                    raw = scipy.misc.imread(ch)
                    raw = scipy.misc.imresize(raw, (28, 28))
                    raws.append(raw)
                data.append(np.asarray(raws))
    data = np.asarray(data)
    os.makedirs('./Data', exist_ok=True)
    np.save('./Data/omniglot.npy', data)
    return data

if __name__ == '__main__':
    read_omniglot()