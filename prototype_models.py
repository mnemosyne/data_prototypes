#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 17 14:55:20 2018

@author: thalita
"""
import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin, TransformerMixin
from sklearn.cluster import KMeans
from sklearn.metrics import accuracy_score

from sklearn.neighbors import NearestCentroid
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sklearn.pipeline import Pipeline, FeatureUnion
from tempfile import mkdtemp
from shutil import rmtree
from sklearn.externals.joblib import Memory

from sklearn.svm import SVC
import os
from sklearn.preprocessing import MinMaxScaler
from tqdm import tqdm


TOL = 1e-3
MAX_ITER = 600
TMP = './tmp/'


def softmax(x, axis=1):
    """Compute softmax values for each sets of scores in x."""
    
    if axis == 1:        
        shape = [-1,1]
    else:
        shape = [1,-1]
    e_x = np.exp(x - np.max(x, axis=axis).reshape(shape))
    return e_x / e_x.sum(axis=axis).reshape(shape)
        

class MyKMeans(KMeans):
    """
    Scikit learn Pipeline will call fit_transform for this one.
    fit_transform in kmeans calls fit(X)._transform(X)
    Calling predict(X) still returns clusters based on simple distances.
    """
    def __init__(self, n_clusters=3, class_weight=0.0,
                 init='k-means++', n_init=10,
                 max_iter=300, tol=1e-4, precompute_distances='auto',
                 verbose=0, random_state=None, copy_x=True,
                 n_jobs=1, algorithm='auto'):
        KMeans.__init__(self, n_clusters=n_clusters, init=init, n_init=n_init,
                 max_iter=max_iter, tol=tol,
                 precompute_distances=precompute_distances,
                 verbose=verbose, random_state=random_state, copy_x=copy_x,
                 n_jobs=n_jobs, algorithm=algorithm)
        self.class_weight = class_weight

    def fit(self, X, y=None):
        if self.class_weight > 0.0:
            n_classes = np.max(y) + 1
            n_dim = X.shape[1]
            y_one_hot = self.class_weight * np.eye(n_classes)[y]
            X = np.concatenate([X,y_one_hot], axis=1)
            KMeans.fit(self, X)
            self.cluster_centers_class_ = self.cluster_centers_[:, n_dim:]
            self.cluster_centers_ = self.cluster_centers_[:, :n_dim]
        else:
            KMeans.fit(self, X)
        return self

    def fit_transform(self, X, y=None):
        return self.fit(X,y)._transform(X)

    def distances(self, X):
        d = KMeans._transform(self, X)
        if np.any(np.isnan(d)):
            raise RuntimeError("Found nan values in distances to prototypes")
        return d

    def predict(self, X):
#         if self.class_weight > 0.0:
#             X = np.concat([X,np.zeros([X.shape[0],self.num_clusters])], axis=1)
        return KMeans.predict(self, X)

    def _transform(self, X):
#         if self.class_weight > 0.0:
#             X = np.concat([X,np.zeros([X.shape[0],self.num_clusters])], axis=1)
        X_new = KMeans._transform(self, X)
        if np.any(np.isnan(X_new)):
            raise RuntimeError("Found nan values in distances to prototypes")
        X_new = np.array(softmax(-X_new), ndmin=2)
        return_dict = dict(X_old=X, X=X_new, protos=self.cluster_centers_)
        if self.class_weight > 0.0:
            cluster_classes = np.argmax(self.cluster_centers_class_, axis=1)
            return_dict.update(cluster_classes=cluster_classes)
        return return_dict

    def transformX(self, X):
        self.transform(X)['X']


class TopClassifier(LogisticRegression):
    def __init__(self, use_shortcut=False, shortcut=0.0,
                 predict_from_cluster=False,
                 penalty='l2', dual=False, tol=1e-4, C=1.0,
                 fit_intercept=True, intercept_scaling=1,
                 random_state=None, solver='liblinear', max_iter=100,
                 multi_class='ovr', verbose=0, warm_start=False, n_jobs=1):
        LogisticRegression.__init__(
                self, penalty=penalty, dual=dual, tol=tol, C=C,
                 fit_intercept=fit_intercept,
                 intercept_scaling=intercept_scaling,
                 random_state=random_state, solver=solver, max_iter=max_iter,
                 multi_class=multi_class, verbose=verbose,
                 warm_start=warm_start, n_jobs=n_jobs)
        self.shortcut = shortcut
        self.use_shortcut = use_shortcut
        self.predict_from_cluster = predict_from_cluster

    def _read_features(self, features):
        X = features['X']
        if self.use_shortcut:
            # X_tilde is X*protos, because X is acctualy q(x)
            X_tilde = np.matmul(X, features['protos'])
            if self.shortcut == 'cat':
                X = np.concatenate([features['X_old'], X_tilde], axis=1)
            else:
            # weight for shortcut is alpha * X_old + (1-alpha) X_tilde
                X_shortcut = self.shortcut * features['X_old']
                X = (1-self.shortcut) * X_tilde
                X = X + X_shortcut
        return X

    def fit(self, features, y, **fit_params):
        if self.predict_from_cluster and 'cluster_classes' in features:
            self.cluster_classes_ = features['cluster_classes']
            return self

        X = self._read_features(features)
        return LogisticRegression.fit(self, X, y, **fit_params)

    def predict(self, features):
        X = self._read_features(features)
        if self.predict_from_cluster:
            cluster_indices = np.argmax(X, axis=1)
            return self.cluster_classes_[cluster_indices]

        return LogisticRegression.predict(self, X)

    def predict_proba(self, features):
        X = self._read_features(features)
        if self.predict_from_cluster:
            cluster_indices = np.argmax(X, axis=1)
            return softmax(self.cluster_classes_[cluster_indices], axis=1)

        return LogisticRegression.predict_proba(self, X)


class MultiPrototypes(Pipeline):
    def __init__(self, random_state, verbose=0, cachedir=None, **kwargs):
        self.random_state = random_state
        pipeline = [('cluster',
                     MyKMeans(n_clusters=20, random_state=random_state))]
        pipeline.append(
            ('classify',
             TopClassifier(random_state=random_state,
                           multi_class='multinomial',
                           solver='lbfgs',
                           tol=TOL,
                           max_iter=MAX_ITER,
                           fit_intercept=False))
        )
        # Create a temporary folder to store the transformers of the pipeline
        os.makedirs(TMP, exist_ok=True)
        self.cachedir = cachedir if cachedir else mkdtemp(dir=TMP)
        self.verbose = verbose
        memory = Memory(cachedir=self.cachedir, verbose=verbose)
        Pipeline.__init__(self, pipeline, memory=memory)
        self.set_params(**kwargs)

    def clear_cache(self):
        rmtree(self.cachedir)


def multiprototypes(random_state, verbose=0, cachedir=None, **kwargs):
    pipeline = [('normalize', MinMaxScaler()),
                ('cluster',
                 MyKMeans(n_clusters=20, random_state=random_state)),
                ('classify',
                 TopClassifier(random_state=random_state,
                               multi_class='multinomial',
                               solver='lbfgs',
                               tol=TOL,
                               max_iter=MAX_ITER,
                               fit_intercept=False))]
    # Create a temporary folder to store the transformers of the pipeline
    # os.makedirs(TMP, exist_ok=True)
    # memory = Memory(cachedir=cachedir if cachedir else mkdtemp(dir=TMP),
    #                 verbose=verbose)
    # pipeline = Pipeline(pipeline, memory=memory)
    pipeline = Pipeline(pipeline)
    return pipeline


class Prototypical(MyKMeans):
    def fit_transform(self, X, y):
        return self.fit(X,y).transform(X)
    def fit(self, X, y):
        prototypes = []
        prototype_class = []
        for k in range(self.n_clusters):
            points = np.ix_(y==k)
            if len(points[0]) == 0:
                raise ValueError("no samples for class %d, n_clusters=%d" % (k, self.n_clusters))
            proto = X[points].mean(axis=0)
            prototypes.append(proto)
            prototype_class.append(k)
        self.cluster_centers_ = np.array(prototypes)
        return self
    def score(self, X, y):
        accuracy_score(y, self.predict(X))


def classify_svm(X, y, N, skf, skf_cv, random_state=0,
                 n_jobs=2,
                 n_Cs=10,
                 compute_probability=False,
                 return_model=False, verbose=True):
    acc=[]
    models = []
    for train_index, test_index in skf.split(X, y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]


        classif = SVC(kernel='rbf',
                      gamma='scale',  # uses 1 / (n_features * X.std())
                      probability=compute_probability,
                      tol=TOL,
                      max_iter=-1,  # -1 == unlimited
                      random_state=random_state)

        normalize = MinMaxScaler()

        estimator = Pipeline([('normalize', normalize), ('classif', classif)])
        param_grid = [{'classif__C': np.logspace(-4, +4, n_Cs, base=10)}]
        model = GridSearchCV(estimator, param_grid, cv=skf_cv,
                             scoring='accuracy',
                             verbose=1,
                             iid=False,  # default from sklearn 0.21
                             n_jobs=n_jobs, return_train_score=True)

        model.fit(X_train,y_train)
        acc.append(model.score(X_test, y_test))
        models.append(model)
        if verbose: print("accuracy on test", acc[-1])
    if verbose: print("average test accuracy", np.mean(acc), np.std(acc))
    if return_model:
        return acc, models
    else:
        return acc


def classify_softmax(X, y, N, skf, skf_cv, random_state=0,
                     n_jobs=2, n_Cs=10,
                     return_model=False, verbose=True):
    acc=[]
    models = []
    for train_index, test_index in skf.split(X, y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]

        classif = LogisticRegression(multi_class='multinomial',
                                     solver='lbfgs',
                                     tol=TOL,
                                     max_iter=MAX_ITER,
                                     random_state=random_state)
        normalize = MinMaxScaler()

        estimator = Pipeline([('normalize', normalize),
                              ('classify', classif)])
        param_grid = [{'classify__C': np.logspace(-4, +4, n_Cs, base=10)}]
        model = GridSearchCV(estimator, param_grid, cv=skf_cv,
                             scoring='accuracy',
                             verbose=1,
                             iid=False,  # default from sklearn 0.21
                             n_jobs=n_jobs, return_train_score=True)

        model.fit(X_train,y_train)
        acc.append(model.score(X_test, y_test))
        models.append(model)
        if verbose: print("accuracy on test", acc[-1])
    if verbose: print("average test accuracy", np.mean(acc), np.std(acc))
    if return_model:
        return acc, models
    else:
        return acc


def classify_prototypical(X, y, N, skf, skf_cv, random_state=0,
                          return_model=False, verbose=True):

    acc=[]
    models=[]
    for train_val_index, test_index in skf.split(X, y):
        X_train_val, X_test = X[train_val_index], X[test_index]
        y_train_val, y_test = y[train_val_index], y[test_index]
#        for train_index, val_index in skf_cv.split(X_train_val,y_train_val):
#            X_train, X_val = X_train_val[train_index], X_train_val[val_index]
#            y_train, y_val = y_train_val[train_index], y_train_val[val_index]

        model = Pipeline([('normalize', MinMaxScaler()),
                          ('classif', NearestCentroid())])

        model.fit(X_train_val, y_train_val)
        pred = model.predict(X_test)
        acc.append(accuracy_score(y_test, pred))
        if verbose: print("accuracy on test", acc[-1])
        models.append(model)
    if verbose: print("average test accuracy", np.mean(acc), np.std(acc))
    if return_model:
        return acc, models
    else:
        return acc


def classify_cluster_softmax(X, y, N, skf, skf_cv,
                             n_jobs=2,
                             n_Cs=10, Cs_lim=(2, 3),
                             n_n_clusters=20, n_clusters_lim=(1, 3),
                             n_class_weights=10, class_weight_lim=(0.0, 3.0),
                             n_shortcut=10, shortcut_lim=(-4, 0),
                             shortcut_scale='linear',
                             random_state=0,
                             models_to_test=None,
                             return_model=False, verbose=True):


    pipeline = multiprototypes(random_state=random_state, verbose=0)

    C_space = np.logspace(*Cs_lim, num=n_Cs, base=10)
    n_clusters_lim = tuple(N*np.array(n_clusters_lim))
    n_clusters_space = np.array(np.linspace(*n_clusters_lim, num=n_n_clusters),
                                dtype=int)
    class_weight_space = np.linspace(*class_weight_lim, n_class_weights)
    if shortcut_scale == 'linear':
        shortcut_space = np.linspace(*shortcut_lim, num=n_shortcut)
    elif shortcut_scale == 'log':
        shortcut_space = np.logspace(*shortcut_lim, n_shortcut, base=10)
    # multinomial is available on lbfgs and saga solvers
    # lbfgs does not handle l1
    cluster_params_dict = {
            'cluster__n_clusters': n_clusters_space,
            'cluster__class_weight': class_weight_space,
        }

    param_grid_l2_full = {
            'classify__penalty': ['l2'],
            'classify__solver': ['lbfgs'],
            'classify__C': C_space,
            'classify__shortcut': shortcut_space,
            'classify__use_shortcut': [True]
        }
    param_grid_l2_full.update(cluster_params_dict)

    param_grid_l2_shortcut = param_grid_l2_full.copy()
    param_grid_l2_shortcut.update({'cluster__class_weight': [0.0]})

    param_grid_l2_cat = param_grid_l2_shortcut.copy()
    param_grid_l2_cat.update({'classify__shortcut': ['cat']})


    param_grid_l2_classweight = param_grid_l2_full.copy()
    param_grid_l2_classweight.update({'classify__shortcut': [0.0],
                                     'classify__use_shortcut': [False]})

    param_grid_l2_plain = param_grid_l2_shortcut.copy()
    param_grid_l2_plain.update({'classify__shortcut': [0.0],
                                'classify__use_shortcut': [False]})

    param_grid_l1_full = {
            'classify__penalty': ['l1'],
            'classify__solver': ['saga'],
            'classify__tol': [0.1],
            'classify__C': C_space,
            'classify__shortcut': shortcut_space,
            'classify__use_shortcut': [True]
        }
    param_grid_l1_full.update(cluster_params_dict)

    param_grid_l1_shortcut = param_grid_l1_full.copy()
    param_grid_l1_shortcut.update({'cluster__class_weight': [0.0]})

    param_grid_l1_cat = param_grid_l1_shortcut.copy()
    param_grid_l1_cat.update({'classify__shortcut': ['cat']})

    param_grid_l1_classweight = param_grid_l1_full.copy()
    param_grid_l1_classweight.update({'classify__shortcut': [0.0],
                                     'classify__use_shortcut': [False]})

    param_grid_l1_plain = param_grid_l1_shortcut.copy()
    param_grid_l1_plain.update({'classify__shortcut': [0.0],
                                'classify__use_shortcut': [False]})

    direct_classify_grid = {
            'cluster__n_clusters': n_clusters_space,
            'cluster__class_weight': class_weight_space[1:],
            'classify__shortcut': [0],
            'classify__use_shortcut': [False],
            'classify__predict_from_cluster': [True],
        }

    params = dict(
        l1_full=param_grid_l1_full,
        l1_classweight=param_grid_l1_classweight,
        l1_shortcut=param_grid_l1_shortcut,
        l1_cat=param_grid_l1_cat,
        l1_plain=param_grid_l1_plain,
        l2_full=param_grid_l2_full,
        l2_classweight=param_grid_l2_classweight,
        l2_shortcut=param_grid_l2_shortcut,
        l2_cat=param_grid_l2_cat,
        l2_plain=param_grid_l2_plain,
        direct=direct_classify_grid)

    def run(param_grid):
        param_grid = [param_grid]
        acc = []
        models = []
        for train_index, test_index in skf.split(X, y):
            X_train, X_test = X[train_index], X[test_index]
            y_train, y_test = y[train_index], y[test_index]
            model = GridSearchCV(pipeline, param_grid, cv=skf_cv,
                                 verbose=1,
                                 iid=False,  # default from sklearn 0.21
                                 scoring='accuracy',
                                 n_jobs=n_jobs, return_train_score=True)
            model.fit(X_train, y_train)
            #print(model.cv_results_)
            acc += [model.best_estimator_.score(X_test, y_test)]
            models += [model]
            if verbose:
                print("best Params", model.best_params_)
                print("mean acuracy of best cv model: ", model.best_score_)
                print("accuracy on test", acc[-1])
        if verbose:
            print("average test accuracy", np.mean(acc), np.std(acc))
        if return_model:
            return acc, models
        else:
            return acc

    result = {}
    if models_to_test is not None:
        for model in models_to_test:
            result.update({model: run(params[model])})
    else:
        # if none, defaults to running all
        result = dict(
                l1_full=run(param_grid_l1_full),
                l1_classweight=run(param_grid_l1_classweight),
                l1_shortcut=run(param_grid_l1_shortcut),
                l1=run(param_grid_l1_plain),
                l2_full=run(param_grid_l2_full),
                l2_classweight=run(param_grid_l2_classweight),
                l2_shortcut=run(param_grid_l2_shortcut),
                l2=run(param_grid_l2_plain),
                direct=run(direct_classify_grid))

    # Delete the temporary cache before exiting
    # pipeline.clear_cache()
    if pipeline.memory is not None:
        rmtree(pipeline.memory.cachedir)
    return result
