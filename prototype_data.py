#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  2 11:23:52 2018

@author: thalita
"""

import numpy as np
from keras.datasets import mnist, cifar10
import matplotlib.pyplot as plt
from sklearn.model_selection import (StratifiedShuffleSplit,
                                     RepeatedStratifiedKFold)

from prototype_plots import plot_montage
from prototype_utils import StratifiedShufflePredefinedTestSplit


def get_dataset(name, random_seed=None):
    DATASETS = {'mnist': Mnist,
                'cifar10': Cifar10,
                'omniglot': Omniglot}
    return DATASETS[name](random_seed)


class Dataset():
    def __init__(self, random_seed=None):
        self.random_seed = random_seed

    def gen_data(self, N=10, plot=False, random_state=None):
        if self.random_seed is not None:
            np.random.seed(self.random_seed)
        if random_state is not None:
            np.random.set_state(random_state)

        X_imgs, X, y = self._load_data(N=N)
        if plot:
            self._plot(X_imgs)

        return X_imgs, X, y

    def _plot(self, images, **kwargs):
        plot_montage(images, **kwargs)

    def splitters(self, train_size=0.8, n_splits=1,
                  cv_folds=2, cv_repeats=1):
        skf = self._top_splitter(n_splits=n_splits, train_size=train_size)
        skf_cv = self._cv_splitter(cv_folds=cv_folds, cv_repeats=cv_repeats)
        return skf, skf_cv

    def _top_splitter(self, n_splits=1, train_size=0.5):
        return StratifiedShuffleSplit(n_splits=n_splits,
                                      train_size=train_size,
                                      test_size=None,
                                      random_state=self.random_seed)

    def _cv_splitter(self, cv_folds=2, cv_repeats=5):
        return RepeatedStratifiedKFold(
                n_splits=cv_folds, n_repeats=cv_repeats,
                random_state=self.random_seed)


class Omniglot(Dataset):
    def gen_data(self, N=10, plot=False, random_state=None):
        if self.random_seed is not None:
            np.random.seed(self.random_seed)
        if random_state is not None:
            np.random.set_state(random_state)
        features = np.load('./Data/omniglot_inceptionV3_features.npy')
        n_classes, n_samples, n_features = features.shape
        classes = np.random.choice(np.arange(n_classes), size=N,
                                   replace=False)
        images = np.load('./Data/omniglot.npy')
        imgs = images[classes, ... ]  # (N classes, n_samples, 28, 28)
        if plot:
            n_imgs = 5
            im = imgs[:, 0:n_imgs, :, :].transpose(1,0,2,3).reshape([-1,28,28])
            self._plot(im, max_imgs=n_imgs*N, grid_shape=(n_imgs, N),
                       cmap='Greys_r')

#            for i in range (N):
#                for j in range(5):
#                    plt.subplot2grid((5, N), (j, i))
#                    if j == 0:
#                        plt.title(str(i+1), fontsize='x-small')
#                    img = imgs[i, j, ... ].squeeze()
#                    plt.imshow(img, cmap='Greys_r')
#                    plt.axis('off')
#            plt.tight_layout()
        X_imgs = imgs.reshape(N*n_samples,28,28)
        X = np.reshape(features[classes, :, :], (N*n_samples, n_features))
        y = np.repeat(np.arange(N), n_samples)
        return X_imgs, X, y



    def _top_splitter(self, n_splits=1, train_size=None):
        return RepeatedStratifiedKFold(
                n_splits=2,
                n_repeats=max(1, n_splits//2),
                random_state=self.random_seed)


class Mnist(Dataset):
    def _load_data(self, N=10):
        (x_train, y_train), (x_test, y_test) = mnist.load_data()
        X_imgs = np.concatenate([x_train, x_test])
        y = np.concatenate([y_train, y_test]).squeeze()
        X = np.load('./Data/mnist_inceptionV3_features.npy')
        return X_imgs, X, y

    def _plot(self, images, **kwargs):
        super()._plot(images, cmap='Greys', **kwargs)

    def _top_splitter(self, train_size=100, n_splits=1):
        test_fold = np.array(60000*[-1]+10000*[0])
        skf = StratifiedShufflePredefinedTestSplit(
                test_fold=test_fold, train_size=train_size,
                n_splits=n_splits, random_state=self.random_seed)
        return skf


class Cifar10(Dataset):
    def _load_data(self, N=10):
        (x_train, y_train), (x_test, y_test) = cifar10.load_data()
        X_imgs = np.concatenate([x_train, x_test])
        X = np.load('./Data/cifar10_inceptionV3_features.npy')
        y = np.concatenate([y_train, y_test]).squeeze()
        #    if N <10:
        #        class_subset_mask = np.isin(y, classes)
        #        X = X[class_subset_mask]
        #        X_imgs = X_imgs[class_subset_mask]
        #        y = y[class_subset_mask]

        return X_imgs, X, y

    def _top_splitter(self, train_size=100, n_splits=1):
        test_fold = np.array(50000*[-1]+10000*[0])
        skf = StratifiedShufflePredefinedTestSplit(
                test_fold=test_fold, train_size=train_size,
                n_splits=n_splits, random_state=self.random_seed)
        return skf
