#!/usr/bin/env bash
# Inform to sbatch the comma separated list of training sizes
# either here or with --array=10,20
##SBATCH --array=5,10,50,100
#Job name
#SBATCH -Jprototype
# Asking for one node
#SBATCH -N1
#SBATCH --ntasks 24


NJOBS=$SLURM_NTASKS
data=cifar10

python3 prototype_main.py --exp train_size --dataset $data --n_splits 10 --n_jobs $NJOBS --train_size $SLURM_ARRAY_TASK_ID 


