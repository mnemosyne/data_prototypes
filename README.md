Code for "Bio-inspired analysis of deep learning on not-so-big data using data-prototypes".

Before running experiments on omniglot dataset, download it from: (https://github.com/brendenlake/omniglot/tree/master/python).
Then run:
```
make_omniglot_npy.py
inceptionV3_features.py omniglot
```
For mnist and cifar10 do:
```
inceptionV3_features.py mnist
inceptionV3_features.py cifar10
```

To run all experiments run `sbatch.sh`

To run a specific experiment:
```
./prototype_main.py --exp episodes --n_episodes 100 --n_classes 10
./prototype_main.py --exp train_size --n_splits 10 --dataset mnist
./prototype_main.py [--exp custom] --dataset omniglot --train_size 0.5 --n_splits 2
```

- `--exp episodes` runs the multi-episode experiment on omniglot.
- `--exp train_size` runs an experiment with a given `train_size` for the given `--dataset`.
