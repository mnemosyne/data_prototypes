#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 17 20:05:59 2018

@author: thalita
"""
import numpy as np
import pandas as pd

from sklearn.neural_network import MLPClassifier as sklearnMLP
from sklearn.model_selection import (RepeatedStratifiedKFold,
                                     StratifiedShuffleSplit,
                                     PredefinedSplit)


class StratifiedShufflePredefinedTestSplit(StratifiedShuffleSplit,
                                           PredefinedSplit):
    def __init__(self, test_fold, train_size=0.1,
                 n_splits=1, random_state=None):
        StratifiedShuffleSplit.__init__(
                self, n_splits=n_splits,
                train_size=train_size,
                test_size=None,  # complements train_size
                random_state=random_state)
        PredefinedSplit.__init__(self, test_fold)

    def split(self, X, y):
        train, test = next(PredefinedSplit.split(self, X, y))
        for train, _ in StratifiedShuffleSplit.split(self, X[train], y[train]):
            yield train, test


def load_results(path):
    struct = pd.read_pickle(path)
    results = struct['results']
    exp_info = struct['info']
    return results, exp_info


def sample_class_subset(x_train, y_train, x_test, y_test, N=5, classes=None):
    """inform either N<10 for a random number of classes or
    classes = list for a list of specific class indices"""
    if not isinstance(classes, list) and N < 10:
        classes = np.random.choice(np.arange(0,10+1), N, replace=False)
    if classes is not None:
        filt = lambda x, y : x[np.in1d(y,classes)]
        x_train = filt(x_train, y_train)
        y_train = filt(y_train, y_train)
        x_test = filt(x_test, y_test)
        y_test = filt(y_test, y_test)


def gen_exception(X, y, class_id, n=2, margin=0.1, radius=0.1):
    """class_id: list of ids"""
    Xs = []
    ys = []
    for c in class_id:
        class_Xs = X[y==c]
        class_center = np.mean(class_Xs, axis=0)
        class_dmax = np.max(np.linalg.norm(class_Xs - class_center, axis=1))
        exception_center = class_center + class_dmax * (margin)
        X_ex = exception_center + np.random.rand(n, 1) * radius * class_dmax
        y_ex = np.zeros(n)+c
        Xs.append(X_ex)
        ys.append(y_ex)
    X_ex = np.concatenate(Xs, axis=0)
    y_ex = np.concatenate(ys, axis=0)
    return X_ex, y_ex


class MLPClassifier(sklearnMLP):

    def transform(self, X):
        """Return activations using the trained model

        Parameters
        ----------
        X : {array-like, sparse matrix}, shape (n_samples, n_features)
            The input data.

        Returns
        -------
        activations : list of N_layer-2 arrays, shape (n_samples, n_units)

        """
#        X = check_array(X, accept_sparse=['csr', 'csc', 'coo'])

        # Make sure self.hidden_layer_sizes is a list
        hidden_layer_sizes = self.hidden_layer_sizes
        if not hasattr(hidden_layer_sizes, "__iter__"):
            hidden_layer_sizes = [hidden_layer_sizes]
        hidden_layer_sizes = list(hidden_layer_sizes)

        layer_units = [X.shape[1]] + hidden_layer_sizes + \
            [self.n_outputs_]

        # Initialize layers
        activations = [X]

        for i in range(self.n_layers_ - 1):
            activations.append(np.empty((X.shape[0],
                                         layer_units[i + 1])))
        # forward propagate
        # appends to activations
        self._forward_pass(activations)

        return activations[1:-1]
