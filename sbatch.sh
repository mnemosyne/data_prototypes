#!/usr/bin/env bash
# This script calls all runs sequentially
# It is usually too long to run this way and not feasible
# keeping it for the record
# should use the sbatch_array scripts instead
#Job name
#SBATCH -Jprototype
# Asking for one node
#SBATCH -N1
#SBATCH --ntasks 24


NJOBS=$SLURM_NTASKS

for N in 10 20 30
do
    python3 prototype_main.py --exp episodes --n_episodes 100 --n_jobs $NJOBS --n_classes $N
done


for data in cifar10 mnist
do
    for N in 5 10 15 20 50 100 250 500 750
    do 
	python3 prototype_main.py --exp train_size --dataset $data --n_splits 10 --n_jobs $NJOBS --train_size $N
    done
done


