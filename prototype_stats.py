#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  1 18:33:23 2018

@author: thalita

Functions for statistical testing
"""
from scipy.stats import ttest_ind, wilcoxon
from scipy.stats import friedmanchisquare
import scipy as sp
import scipy.stats as st
import numpy as np
#from stac import friedman_test
import itertools as it


def friedman_test(*args):
    """
        From STAC library, BSD 2 license.
        See:
        - https://github.com/citiususc/stac
        - https://gitlab.citius.usc.es/ismael.rodriguez/stac
        ------------------------------------------------------------------------

        Performs a Friedman ranking test.
        Tests the hypothesis that in a set of k dependent samples groups (where k >= 2) at least two of the groups represent populations with different median values.

        Parameters
        ----------
        sample1, sample2, ... : array_like
            The sample measurements for each group.

        Returns
        -------
        F-value : float
            The computed F-value of the test.
        p-value : float
            The associated p-value from the F-distribution.
        rankings : array_like
            The ranking for each group.
        pivots : array_like
            The pivotal quantities for each group.

        References
        ----------
        M. Friedman, The use of ranks to avoid the assumption of normality implicit in the analysis of variance, Journal of the American Statistical Association 32 (1937) 674–701.
        D.J. Sheskin, Handbook of parametric and nonparametric statistical procedures. crc Press, 2003, Test 25: The Friedman Two-Way Analysis of Variance by Ranks
    """
    k = len(args)
    if k < 2:
        raise ValueError('Less than 2 levels')
    n = len(args[0])
    if len(set([len(v) for v in args])) != 1:
        raise ValueError('Unequal number of samples')

    rankings = []
    for i in range(n):
        row = [col[i] for col in args]
        row_sort = sorted(row)
        rankings.append(
                [row_sort.index(v) + 1 + (row_sort.count(v)-1)/2. for v in row])

    rankings_avg = [sp.mean([case[j] for case in rankings]) for j in range(k)]
    rankings_cmp = [r/sp.sqrt(k*(k+1)/(6.*n)) for r in rankings_avg]

    chi2 = ((12*n)/(k*(k+1)))*((sp.sum(r**2 for r in rankings_avg))-((k*(k+1)**2)/4.0))
    iman_davenport = ((n-1)*chi2)/(n*(k-1)-chi2)

    p_value = 1 - st.f.cdf(iman_davenport, k-1, (k-1)*(n-1))

    return iman_davenport, p_value, rankings_avg, rankings_cmp



def friedman_aligned_ranks_test(*args):
    """
        From STAC library, BSD 2 license.
        See:
        - https://github.com/citiususc/stac
        - https://gitlab.citius.usc.es/ismael.rodriguez/stac
        ------------------------------------------------------------------------

        Performs a Friedman aligned ranks ranking test.
        Tests the hypothesis that in a set of k dependent samples groups (where k >= 2) at least two of the groups represent populations with different median values.
        The difference with a friedman test is that it uses the median of each group to construct the ranking, which is useful when the number of samples is low.

        Parameters
        ----------
        sample1, sample2, ... : array_like
            The sample measurements for each group.

        Returns
        -------
        Chi2-value : float
            The computed Chi2-value of the test.
        p-value : float
            The associated p-value from the Chi2-distribution.
        rankings : array_like
            The ranking for each group.
        pivots : array_like
            The pivotal quantities for each group.

        References
        ----------
         J.L. Hodges, E.L. Lehmann, Ranks methods for combination of independent experiments in analysis of variance, Annals of Mathematical Statistics 33 (1962) 482–497.
    """
    k = len(args)
    if k < 2:
        raise ValueError('Less than 2 levels')
    n = len(args[0])
    if len(set([len(v) for v in args])) != 1:
        raise ValueError('Unequal number of samples')

    aligned_observations = []
    for i in range(n):
        loc = sp.mean([col[i] for col in args])
        aligned_observations.extend([col[i] - loc for col in args])

    aligned_observations_sort = sorted(aligned_observations)

    aligned_ranks = []
    for i in range(n):
        row = []
        for j in range(k):
            v = aligned_observations[i*k+j]
            row.append(aligned_observations_sort.index(v) + 1 + (aligned_observations_sort.count(v)-1)/2.)
        aligned_ranks.append(row)

    rankings_avg = [sp.mean([case[j] for case in aligned_ranks]) for j in range(k)]
    rankings_cmp = [r/sp.sqrt(k*(n*k+1)/6.) for r in rankings_avg]

    r_i = [np.sum(case) for case in aligned_ranks]
    r_j = [np.sum([case[j] for case in aligned_ranks]) for j in range(k)]
    T = (k-1) * (sp.sum(v**2 for v in r_j) - (k*n**2/4.) * (k*n+1)**2) / float(((k*n*(k*n+1)*(2*k*n+1))/6.) - (1./float(k))*sp.sum(v**2 for v in r_i))

    p_value = 1 - st.chi2.cdf(T, k-1)

    return T, p_value, rankings_avg, rankings_cmp

def nemenyi_multitest(ranks):
    """
        From STAC library, BSD 2 license.
        See:
        - https://github.com/citiususc/stac
        - https://gitlab.citius.usc.es/ismael.rodriguez/stac
        ------------------------------------------------------------------------
        Performs a Nemenyi post-hoc test using the pivot quantities obtained by a ranking test.
        Tests the hypothesis that the ranking of each pair of groups are different.

        Parameters
        ----------
        pivots : dictionary_like
            A dictionary with format 'groupname':'pivotal quantity'

        Returns
        ----------
        Comparions : array-like
            Strings identifier of each comparison with format 'group_i vs group_j'
        Z-values : array-like
            The computed Z-value statistic for each comparison.
        p-values : array-like
            The associated p-value from the Z-distribution wich depends on the index of the comparison
        Adjusted p-values : array-like
            The associated adjusted p-values wich can be compared with a significance level

        References
        ----------
        Bonferroni-Dunn: O.J. Dunn, Multiple comparisons among means, Journal of the American Statistical Association 56 (1961) 52–64.
    """

    k = len(ranks)
    values = list(ranks.values())
    keys = list(ranks.keys())
    versus = list(it.combinations(range(k), 2))

    comparisons = [keys[vs[0]] + " vs " + keys[vs[1]] for vs in versus]
    z_values = [abs(values[vs[0]] - values[vs[1]]) for vs in versus]
    p_values = [2*(1-st.norm.cdf(abs(z))) for z in z_values]
    # Sort values by p_value so that p_0 < p_1
    p_values, z_values, comparisons = map(list, zip(*sorted(zip(p_values, z_values, comparisons), key=lambda t: t[0])))
    m = int(k*(k-1)/2.)
    adj_p_values = [min(m*p_value,1) for p_value in p_values]

    return comparisons, z_values, p_values, adj_p_values


def friedman_nemenyi(names, *measurements, use_median=True):
    if use_median:
        test_fn = friedman_aligned_ranks_test
    else:
        test_fn = friedman_test
    F, p, ranks, pivots = test_fn(*measurements)
    pivots_dict = dict(list(zip(names,pivots)))
    return nemenyi_multitest(pivots_dict)

def test_all_pairs(res_dict):
    test = dict()
    pri = dict(zip(res_dict.keys(),range(len(res_dict))))
    for method1, df1 in res_dict.items():
        for method2, df2 in res_dict.items():
            if method1 != method2 and pri[method1] < pri[method2]:
                res1 = df1.holdout_test_accuracy
                res2 = df2.holdout_test_accuracy
                _, pval = ttest_ind(res1, res2, equal_var=False)
                test[(method1, method2)] = {'ttest': pval}
                _, pval = wilcoxon(res1, res2)
                test[(method1, method2)]['wilcoxon'] = pval
    return test