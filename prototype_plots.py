#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 16:53:13 2018

@author: thalita
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import os
import glob

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import MinMaxScaler
from sklearn.pipeline import Pipeline
from sklearn.base import BaseEstimator
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE, Isomap

from IPython.display import display, HTML, Markdown

import pandas as pd

from scipy.stats import entropy
from itertools import combinations

from skimage.util import montage
if not callable(montage):
    from skimage.util.montage import montage2d as montage

from prototype_utils import load_results
from prototype_stats import friedman_nemenyi
from prototype_models import MyKMeans


def model_rename(name):
    names = {'l1_plain' : '*direct L1',
             'l2_plain' : '*direct L2',
             'single_prototype': 'nearest centroids',
             'l1_cat' : '*combined (L1)',
             'l2_cat' : '*combined (L2)',
             'svm': 'SVM',
             'softmax': 'softmax',
             'n_episodes': 'episodes',
             'train_size': 'training set samples',
             'n_splits': 'random training subsets',
             'n_classes': 'classes'}

    return names[name]


def savefig_eps(filepath, **kwargs):
    # allow legend transparency
    ax = plt.gcf().gca()
    ax.set_rasterization_zorder(1)
    leg = ax.get_legend()
    if leg is not None:
        leg.set_zorder(0)
        leg.set_rasterized(True)
        leg.get_frame().set_alpha(0.8)
    if not filepath.endswith('.eps'):
        filepath += '.eps'
    plt.savefig(filepath,
                rasterized=True,
                dpi=300,
                **kwargs)

def report_stats(models, *measurements, alpha=0.05):
        """
        models: list of model names
        *measurements: lists of scores for each model
        """
        (comparisons,
         z_values, p_values,
         adj_p_values) = friedman_nemenyi(models, *measurements)

        winners = []
        measurements_dict = dict(zip(models, measurements))
        table = pd.DataFrame(index=models, columns=models)
        for pair, adj_p_val in zip(comparisons, adj_p_values):
            m1, m2 = pair.split(' vs ')
            m1_median = np.median(measurements_dict[m1])
            m2_median = np.median(measurements_dict[m2])
            winner = m1 if m1_median > m2_median else m2
            winners.append(winner)
            if adj_p_val < 0.01:
                mark = '++'
            elif 0.01 < adj_p_val < alpha:
                mark='+'
            else:
                mark =  'o'
            table.at[m1, m2] = table.at[m2, m1] = mark
        for model in models:
            table.at[model, model] = '-'

        df = pd.DataFrame({
               'comparisons': comparisons,
               'winner': winners,
               #'p_values': p_values,
               'adj_p_values': adj_p_values})
        #display(Markdown("#### H0 not rejected:"))
        #display(HTML(df.loc[df.adj_p_values>=alpha, : ].to_html()))
        display(Markdown("#### H0 rejected:"))
        display(HTML(df.loc[df.adj_p_values<alpha,['comparisons','winner']].to_html()))
        display(Markdown("#### Summary table"))
        display(HTML(table.to_html()))






def _generic_stats_boxplot(dataset, result_files, param_name, fixed_param_name):
    #plt.rc('text', antialiased=True)
    nrows = len(result_files)
    plt.figure('boxplots', figsize=(9, 2.3*nrows), dpi=300)
    res_df = pd.DataFrame()

    params_file = dict() # param - result file correspondance
    for i, r in enumerate(result_files):
        results, exp_info = load_results(r)
        param = exp_info[param_name]
        params_file[param] = r

    ordered_params = sorted(params_file.keys())

    for i, param in enumerate(ordered_params):
        r = params_file[param]
        results, exp_info = load_results(r)
        param = exp_info[param_name]

        models = sorted(results.keys())
        d = dict([(model_rename(m), results[m].holdout_test_accuracy)
                  for m in models])
        models = d.keys()
        df = pd.DataFrame(d)

        # stat test
        measurements = tuple([d[m] for m in models])

        title = "%d %s" % (param, model_rename(param_name))
        display(Markdown(('-'*50)))
        display(Markdown(("### " + title)))
        report_stats(models, *measurements)
        # plots - reuse title
        plt.subplot(nrows, 1, i+1)
        ax = df.boxplot(return_type='axes')
        plt.ylabel(title)
        if dataset == 'omiglot':
            plt.ylim((0.2, 1))
        elif dataset == 'mnist':
            plt.ylim((0.4, 1))
        else:
            plt.ylim((0,1))
        y_low, y_high = plt.ylim()
        y_delta = y_high - y_low
        medians = df.median()
        labels = ax.get_xticklabels()
        labels = [l.get_text() for l in labels]
        for ix, l in enumerate(labels):
            ax.annotate("%0.2f" % medians.loc[l],
                        xy=(ix+0.9, y_low+0.05*y_delta))

        res_df = res_df.append(df.assign(param=exp_info[param_name]),
                               ignore_index=True)
    res_df = res_df.rename(columns={'param': param_name})


    suptitle = dataset.capitalize()
    suptitle += ' - mean test accuracy - %d %s' % (exp_info[fixed_param_name],
                              model_rename(fixed_param_name))
    plt.suptitle(suptitle, y=1.0)
    plt.tight_layout()

    base_path = '/'.join(r.split('/')[0:-2])
    plt.savefig(os.path.join(base_path, dataset+'_boxplot.pdf'), dpi=300)

    mean_data = res_df.groupby(param_name).mean()
    mean_data.plot.line(yerr=res_df.groupby(param_name).std(), style='+:')
    plt.gcf().set_size_inches(6, 4)
    plt.gcf().set_dpi(300)
    plt.xlabel(model_rename(param_name))
    plt.ylabel('mean test accuracy')
    plt.xticks(sorted(res_df[param_name].values.tolist()))
    title = dataset.capitalize()
    title += ' - mean accuracy on test set'
    title += '\n(%d %s)' % (exp_info[fixed_param_name],
                            model_rename(fixed_param_name))
    plt.title(title)
    plt.legend(loc='lower right')
    plt.savefig(os.path.join(base_path, dataset+'_comparison.pdf'), dpi=300)
    return res_df


def report_episodes(dataset='omniglot'):
    path = dataset + '_episodes'

    result_files = glob.glob('**/%s/N*classes_*epi/results.pd.pkl' % path,
                             recursive=True)

    _generic_stats_boxplot(dataset, result_files,
                           'n_classes', 'n_episodes')



def report_train_sizes(dataset):
    path = dataset

    result_files = glob.glob('**/%s/*_splits_*_train_size/*pkl' % path,
                             recursive=True)
    _generic_stats_boxplot(dataset, result_files,
                           'train_size', 'n_splits')


def plot_elbow_curve(X, y, n_clusters=None, cv=None, normalize=False,
                     data_split='test', detailed_results=False,
                     random_state=None, plot=True, **plot_kwargs):
    if n_clusters is None:
        n_clusters = np.arange(10, 50+1, 5)

    steps = []
    if normalize:
        if isinstance(normalize, BaseEstimator):
            step = normalize()
        else:
            step = MinMaxScaler()
        steps.append(('normalize', step))
    steps.append(('cluster', MyKMeans(random_state=random_state)))
    estimator = Pipeline(steps)
    search = GridSearchCV(
        estimator=estimator,
        cv=cv, iid=False,
        param_grid=[{'cluster__n_clusters': n_clusters}])
    search.fit(X, y)
    # scores: rows are splits and columns are n_clusters
    scores = []
    score_suffix = data_split + '_score'
    for k in search.cv_results_:
        if k.startswith('split') and k.endswith(score_suffix):
            scores.append(search.cv_results_[k])
    # scores sent by kmeans are -(sum of squared distances to closet centroid)
    # invert sign and average over samples
    train_sizes = [train.size for train, test in cv.split(X,y)]
    scores = -np.array(scores)
    for i in range(scores.shape[0]):
        scores[i,:] /= train_sizes[i]
    mean = scores.mean(axis=0)
    std = scores.std(axis=0)
    n_clusters = search.cv_results_['param_cluster__n_clusters'].data
    if plot:
        plt.errorbar(n_clusters, mean, std, linestyle=':', **plot_kwargs)
        plt.title('K-means objective vs number of prototypes')
        plt.xlabel('n prototypes')
        plt.ylabel('Mean of squared distances\nto the assigned prototype')
    if detailed_results:
        return n_clusters, scores
    else:
        return n_clusters, mean, std

def get_montage(imgs, max_imgs=25, grid_shape=None):
    if imgs.ndim == 4:  # color images
        channels = []
        for i in range(imgs.shape[-1]):
            im = montage(imgs[0:max_imgs,:,:,i], grid_shape=grid_shape)
            channels.append(im)
        im = np.stack(channels, axis=-1).astype(np.uint8)
    else:
        im = montage(imgs[0:max_imgs,...], grid_shape=grid_shape, fill=255)
    return im

def plot_montage(imgs, max_imgs=25, **kwargs):
    grid_shape = None
    if 'grid_shape' in kwargs:
        grid_shape = kwargs['grid_shape']
        del kwargs['grid_shape']

    im = get_montage(imgs, max_imgs, grid_shape)

    plt.imshow(im, **kwargs)
    plt.axis('off')


def plot2D(X, y, y_pred=None, N=10, method='pca', n_components=2,
           plot_n_samples=None,**method_kwargs):
    if method == 'pca':
        estim = PCA(n_components, **method_kwargs)
    elif method == 'tsne':
        estim = TSNE(n_components=2,**method_kwargs)
        if plot_n_samples is None:
            plot_n_samples = 100
    elif method == 'lda':
        estim = LinearDiscriminantAnalysis(n_components=2,**method_kwargs)
    elif method == 'isomap':
        estim = Isomap(n_components=2, **method_kwargs)

    # subsample points for plots so it doesn't take forever
    if plot_n_samples is not None and X.shape[0] > plot_n_samples:
        subsample = np.random.choice(np.arange(X.shape[0]),
                                 size=plot_n_samples,
                                 replace=False)
        X, y = X[subsample], y[subsample]
        if y_pred is not None:
            y_pred = y_pred[subsample]


    if X.shape[-1] > 2:
        if method == 'lda':
             X_new = estim.fit_transform(X, y)
        else:
            X_new = estim.fit_transform(X)
        if method == 'pca':
            print("explained variance ", estim.explained_variance_)
    else:
        X_new = X
    def subplot(i, j):
        colors = []
        for k in range(N):
            if y_pred is not None:
                ms=10
            else:
                ms=7
            points, = np.ix_(y==k)
            lines = plt.plot(X_new[points,i].squeeze(),X_new[points,j].squeeze(), label=str(k+1),
                     marker='o', linestyle='', ms=ms)
            colors.append(lines[0].get_color())
        if y_pred is not None:
            for k in range(N):
                points, = np.ix_((y_pred==k) * (y_pred != y))
                plt.plot(X_new[points,i].squeeze(),X_new[points,j].squeeze(),
                         marker='X', linestyle='', ms=7, color=colors[k],
                         markeredgecolor='k', markeredgewidth=0.2)
        plt.legend(loc='best', fontsize='small', title='classes')

    dim_pairs = list(combinations(range(n_components), 2))
    nplots = len(dim_pairs)
    if nplots == 1:
        subplot(*dim_pairs[0])
    else:
        nrows = (nplots-1) // 3 + 1
        ncols = min(nplots, 3)
        plt.figure()
        for p in range(nplots):
            plt.subplot(nrows, ncols, p+1)
            subplot(*dim_pairs[p])
            plt.title('components %d x %d' % dim_pairs[p])

    if X.shape[-1] > 2:
        return estim


def circle(x, y, radius=0.15):
    from matplotlib.patheffects import withStroke
    circle = Circle((x, y), radius, clip_on=False, zorder=10, linewidth=1,
                    edgecolor='black', facecolor=(0, 0, 0, .0125), linestyle=':',
                    path_effects=[withStroke(linewidth=5, foreground='w')])
    plt.gca().add_artist(circle)


def sparsity(mat, tol=1e-6):
    return 1-np.sum(np.abs(mat)>tol)/mat.size


def plot_imgs(imgs, N):
    plt.figure(figsize=(10, 5))
    for i in range (N):
        for j in range(5):
            plt.subplot2grid((5, N), (j, i))
#            if j == 0:
#                plt.title(str(i+1), fontsize='x-small')
            img = imgs[i*N + j, ... ]
            plt.imshow(img)
            plt.axis('off')


def transform_label(label):
    label = label.replace('param_cluster__', '')
    label = label.replace('param_classify__', '')

    label_map = {'n_clusters': r'n prototypes J',
                 'C': 'inverse regularization C',
                 'shortcut': r'$\alpha$',
                 'class_weight': r'class weight $\beta$'}

    for key, v in label_map.items():
        label = label.replace(key, v)
    return label


class Report:
    def __init__(self, name, result_dict, model, splitter, N, split=0,
                 directory='prototype_results'):
        self.N = N
        self.model = result_dict[model][split]
        self.classify = self.model.best_estimator_.named_steps.classify
        self.cluster = self.model.best_estimator_.named_steps.cluster
        self.normalize = self.model.best_estimator_.named_steps.normalize
        self.cluster_pipe = Pipeline([('normalize', self.normalize),
                                      ('cluster', self.cluster)])
        self.split = split
        self.skf = splitter
        self.use_shortcut = self.classify.use_shortcut
        self.shortcut = self.classify.shortcut
        self._proto_class_proba()
        self._wc_shortcuts()

        self.report_path = os.path.join(directory, name, model + '_%d' % split)
        os.makedirs(os.path.join(directory, name), exist_ok=True)


    def savefig(self, fname, comment=None, **savefig_kwargs):
        fname = '_'.join([self.report_path, fname])
        if fname.endswith('.eps'):
            savefig_eps(fname, **savefig_kwargs)
            metainfo = '\n%% model best_cv_params: ' + str(self.model.best_params_)
            metainfo += '\n%% test split: %d' % self.split
            if comment is not None:
                metainfo += '\n%% ' + comment
            with open(fname, 'a') as f:
                f.write(metainfo)
        else:
            plt.savefig(fname, dpi=300, **savefig_kwargs)

    def report(self, X, y):
        model = self.model
        print("best Params", model.best_params_)
        print("mean acuracy of best cv model: ", model.best_score_)
        train,test = self._split(X,y)
        print("best model train accuracy: ", model.score(X[train], y[train]))
        print("holdout test accuracy: ", model.score(X[test], y[test]))


    def main_parameter_grids(self, fix='all'):
        plots = [dict(index=['param_classify__C'],
                      columns=['param_cluster__n_clusters'],fix=fix)]

        plots.append(dict(
            index=['param_cluster__n_clusters'],
            columns=['param_classify__C'],fix=fix))

        plt.figure(figsize=(10,4), dpi=300)
        for i,p in enumerate(plots):
            ax = plt.subplot(1, len(plots), i+1)
            self.parameter_grid(**p, ax=ax)


    def parameter_grid(self, index, columns, fix=None, plot=True,
                       **plot_kwargs):
        model = self.model
        penalty = model.best_params_['classify__penalty']
        possible_columns = set(['param_classify__C',
                                'param_cluster__n_clusters',
                                'param_cluster__class_weight',
                                'param_classify__shortcut'])
        df = pd.DataFrame(model.cv_results_)
        if fix == 'all':
            fix = possible_columns - set(index+columns)
        elif fix is None:
            fix = []

        for col in fix:
            v = model.best_params_[col.strip('param_')]
            if isinstance(fix, dict):
                if fix[col] != 'best':
                    v = fix[col]
            df = df.loc[df[col]==v, :]

        t = pd.pivot_table(
            df, values='mean_test_score',
            index=index,
            columns=columns,
            aggfunc=np.mean)

        unused_columns = possible_columns - set(index+columns) - set(fix)
        if len(unused_columns) > 0:
            terr =  pd.pivot_table(
                df, values='mean_test_score',
                index=index,
                columns=columns,
                aggfunc=np.std)
        else:
            terr = pd.pivot_table(
                df, values='std_test_score',
                index=index,
                columns=columns,
                aggfunc=np.max)
        if plot:
            if len(index) == 1:
                t.plot(style=[':+',':d',':.',':x']*20, yerr=terr,
                       **plot_kwargs)
            else:
                t.plot.bar(yerr=terr,
                           **plot_kwargs)


            plt.title('mean accuracy %s' % penalty.capitalize())
            if index[0] in ['param_classify__C', 'param_classify__shortcut']:
                plt.xscale('log')
            ax = plt.gca()
            ax.set_xlabel(transform_label(ax.get_xlabel()))

            handles, labels = ax.get_legend_handles_labels()
            if 'param_classify__C' in columns:
                labels = ['%0.0e' % float(l) for l in labels]
            new_title = transform_label(ax.get_legend().get_title().get_text())
            ax.legend(handles, labels,title=new_title)
        return df,t, terr


    def _wc_shortcuts(self):
        self.Wc = coef = self.classify.coef_
        if self.use_shortcut and self.shortcut == 'cat':
            n_features = self.Wc.shape[1]//2
            self.Wc_direct = coef[:, 0:n_features]
            self.Wc_xtilde = coef[:, n_features:]
        elif self.use_shortcut:
            self.Wc = coef[:, 0:self.n_proto]
            self.Wc_shortcut = coef[:, self.n_proto:]


    def _proto_class_proba(self):
        self.protos = self.cluster.cluster_centers_
        self.n_proto = self.cluster.n_clusters
        feature = dict(X=np.eye(self.n_proto),
                       X_old=self.protos,
                       protos=self.protos)
        # classes x centers
        self.cluster_class = self.classify.predict(feature)
        # centers x classes
        self.cluster_proba = self.classify.predict_proba(feature)

    def wc_stats(self):
        print('Wc sparsity @ tol=0.1', sparsity(self.Wc, tol=0.1))
        print('Wc mean: ', self.Wc.mean(), 'std: ', self.Wc.std())

    def params(self):
        print("best parameters: ", self.model.best_params_)

    def visualize_weights(self):
        n_proto = self.n_proto
        plt.figure()
        ncols = 3 if self.use_shortcut else 2
        plt.subplot(1,ncols,1)
        plt.imshow(self.cluster_proba, cmap="RdYlBu_r")
        plt.title('probas')
        plt.colorbar()
        plt.xlabel('classes')
        plt.ylabel('prototypes')
        plt.xticks(np.arange(0,self.N), np.arange(1, self.N+1))
        plt.yticks(np.arange(0,n_proto), np.arange(1, n_proto+1))
        plt.subplot(1,ncols,2)
        if self.shortcut != 'cat':
            # Wc is n_class x n_clusters
            plt.imshow(self.Wc.T, cmap="RdYlBu_r")
            plt.title('$W_l$')
            plt.xlabel('classes')
            plt.ylabel('prototypes')
            plt.xticks(np.arange(0,self.N), np.arange(1, self.N+1))
            plt.yticks(np.arange(0,n_proto), np.arange(1, n_proto+1))
            plt.colorbar()
            if self.use_shortcut:
                plt.subplot(1, ncols, ncols)
                plt.imshow(self.Wc_shortcut.T, cmap="RdYlBu_r")
                plt.title('coeficients for shortcut dimensions')
                plt.axis('tight')
                plt.xlabel('classes')
                plt.ylabel('original feature dimensions')
                plt.xticks(np.arange(0,self.N), np.arange(1, self.N+1))
                plt.colorbar()
        else: # shortcu by concatenation
            plt.imshow(self.Wc_direct.T, cmap="RdYlBu_r")
            plt.axis('tight')
            plt.title('$W_l$ direct')
            plt.xlabel('classes')
            plt.ylabel('features')
            plt.xticks(np.arange(0,self.N), np.arange(1, self.N+1))
            #plt.yticks(np.arange(0,n_proto), np.arange(1, n_proto+1))
            plt.colorbar()
            plt.subplot(1, ncols, ncols)
            plt.imshow(self.Wc_xtilde.T, cmap="RdYlBu_r")
            plt.axis('tight')
            plt.title('$W_l$ reconstruction')
            plt.xlabel('classes')
            plt.ylabel('features')
            plt.xticks(np.arange(0,self.N), np.arange(1, self.N+1))
            #plt.yticks(np.arange(0,n_proto), np.arange(1, n_proto+1))
            plt.colorbar()

        print('proto per class: ', self.cluster_proba.sum(axis=0))
        plt.tight_layout(pad=0.2)


    def _split(self, X, y, X_imgs=None):
        f=0
        for train_index, test_index in self.skf.split(X, y):
            if f == self.split:
                return train_index, test_index
            elif f > self.split:
                raise ValueError("self.split had invalid value: " + str(self.split))
            f += 1

    def entropy_per_sample(self, X, y, split='train'):
        ( X_train, y_train,
         X_test, y_test) = self.get_data(X, y)
        pred_probas = self.model.best_estimator_.predict_proba(X_train).T
        plt.figure()
        plt.subplot(1,2,1)
        plt.plot(pred_probas)
        plt.title('predictions for all samples')
        plt.ylabel('predicted probabilities')
        plt.xlabel('class indices')
        plt.subplot(1,2,2)
        plt.plot(entropy(pred_probas))
        plt.title('entropy of predictions for all samples')
        plt.ylabel('entropy of predictions')
        plt.xlabel('sample indices')


    def get_data(self, X, y, X_imgs=None):
        train_index, test_index = self._split(X, y)
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        if X_imgs is not None:
            return (X_imgs[train_index], X_train, y_train,
                    X_imgs[test_index], X_test, y_test)
        else:
            return X_train, y_train, X_test, y_test

    def closest_images_montage(self, X_imgs, X, y, nimgs=4,
                       ncols=8, show_distances=False):
        "plots montages with closes images for each proto"
        model = self.model
        (X_imgs_train, X_train, y_train,
         X_imgs_test, X_test, y_test) = self.get_data(X, y, X_imgs)

        n_proto = self.n_proto
        imgs = X_imgs_train
        labels = y_train
        preds = model.best_estimator_.predict(X_train)

        X_norm = self.normalize.transform(X_train)
        # distances has shape samples x protos
        distances = self.cluster.distances(X_norm)
        # cluster_idx has shape samples x 1
        cluster_idx = self.cluster.predict(X_norm)
        # cluser_proba has shape protos x class
        cluster_class = np.argmax(self.cluster_proba, axis=1)
        # lists to keep closest and farthest points for each cluster
        # with respective distances (and label for closest)
        closest, closest_d, closest_l, farthest, farthest_d = [],[],[],[],[]
        used_protos = np.zeros(n_proto)
        for c in range(n_proto):
            candidates = np.where(cluster_idx==c)[0]
            used_protos[c] = candidates.size
            # distances has shape samples x protos
            if candidates.size > 0:
                order = np.argsort(distances[candidates, c],
                                                   axis=0)
                farthest_idx = candidates[order[-nimgs:]]
                closest_idx = candidates[order[:nimgs]]
            else:
                order = np.argsort(distances[:, c], axis=0)
                farthest_idx = order[-nimgs:]
                closest_idx = order[:nimgs]

            farthest.append(imgs[farthest_idx,...])
            farthest_d.append(distances[farthest_idx,c])
            closest.append(imgs[closest_idx,...])
            closest_l.append((labels[closest_idx,...], preds[closest_idx,...]))
            closest_d.append(distances[closest_idx,c])


        nrows = int(np.ceil(len(used_protos)/ncols))
        ix = 0
        plt.figure()
        for i in range(nrows):
            for j in range(ncols):
                if ix < n_proto:
                    plt.subplot2grid((nrows, ncols), (i,j))
                    im = get_montage(closest[ix])
                    plt.imshow(im, cmap='Greys_r')
                    l_true, l_pred = closest_l[ix]
                    l_cluster = cluster_class[ix]
                    xlabel = ''
#                    if l_true != l_pred:
#                        xlabel = 'samples c %d\npred %d' % (l_true+1,l_pred+1)
#                    elif l_true != l_cluster:
#                        xlabel = 'samples c %d' % (l_true + 1)
#                    elif l_pred != l_cluster:
#                        xlabel = 'pred %d' % (l_pred + 1)
#                    else:
#                        xlabel = ''
                    #plt.xlabel(xlabel, fontsize='small')
                    plt.axis('off')
                    title = 'p %d - c %d' % (ix+1, l_cluster+1)
                    title += '\n%d samples' % (used_protos[ix])
                    plt.title(title,
                              fontsize='small')
                    plt.tick_params(which='both', length=0,
                                    labelbottom=False, labelleft=False)
                    if show_distances:
                        ylabel = ('$d_{min}=%0.3f$\n$d_{max}=%0.3f$'
                                  % (closest_d[ix].mean(), farthest_d[ix].mean()))
                        plt.ylabel(ylabel, fontsize='small')
                ix += 1
        plt.tight_layout(pad=0.2)

    def closest_images(self, X_imgs, X, y,
                       ncols=8,
                       #true_class=True,
                       show_distances=True):
        model = self.model
        (X_imgs_train, X_train, y_train,
         X_imgs_test, X_test, y_test) = self.get_data(X, y, X_imgs)

        n_proto = self.n_proto
        imgs = X_imgs_train
        labels = y_train
        preds = model.best_estimator_.predict(X_train)

        X_norm = self.normalize.transform(X_train)
        # distances has shape samples x protos
        distances = self.cluster.distances(X_norm)
        # cluster_idx has shape samples x 1
        cluster_idx = self.cluster.predict(X_norm)
        # cluser_proba has shape protos x class
        cluster_class = np.argmax(self.cluster_proba, axis=1)
        # lists to keep closest and farthest points for each cluster
        # with respective distances (and label for closest)
        closest, closest_d, closest_l, farthest, farthest_d = [],[],[],[],[]
        used_protos = np.zeros(n_proto)
        for c in range(n_proto):
            candidates = np.where(cluster_idx==c)[0]
            used_protos[c] = candidates.size

            # XXX often there are no images in the cluster with its class.
#            if true_class:
#                proto_class = cluster_class[c]
#                candidates = filter(lambda x: y[x]==proto_class,candidates)
#                candidates = np.array(list(candidates))

            # distances has shape samples x protos
            if candidates.size > 0:
                farthest_idx = candidates[np.argmax(distances[candidates, c],
                                                    axis=0)]
                closest_idx = candidates[np.argmin(distances[candidates, c],
                                                   axis=0)]
            else:
                farthest_idx = np.argmax(distances[:,c])
                closest_idx = np.argmin(distances[:,c])

            farthest.append(imgs[farthest_idx,...])
            farthest_d.append(distances[farthest_idx,c])
            closest.append(imgs[closest_idx,...])
            closest_l.append((labels[closest_idx,...], preds[closest_idx,...]))
            closest_d.append(distances[closest_idx,c])


        nrows = int(np.ceil(len(used_protos)/ncols))
        ix = 0
        plt.figure()
        for i in range(nrows):
            for j in range(ncols):
                if ix < n_proto:
                    plt.subplot2grid((nrows, ncols), (i,j))
                    plt.imshow(closest[ix], cmap='Greys_r')
                    l_true, l_pred = closest_l[ix]
                    l_cluster = cluster_class[ix]
                    if l_true != l_pred:
                        xlabel = 'sample c %d\npred %d' % (l_true+1,l_pred+1)
                    elif l_true != l_cluster:
                        xlabel = 'sample c %d' % (l_true + 1)
                    elif l_pred != l_cluster:
                        xlabel = 'pred %d' % (l_pred + 1)
                    else:
                        xlabel = ''
                    plt.xlabel(xlabel, fontsize='small')
                    title = 'p %d - c %d' % (ix+1, l_cluster+1)
                    title += '\n%d samples' % (used_protos[ix])
                    plt.title(title,
                              fontsize='small')
                    plt.tick_params(which='both', length=0,
                                    labelbottom=False, labelleft=False)
                    if show_distances:
                        ylabel = '$d_{min}=%0.3f$\n$d_{max}=%0.3f$' % (closest_d[ix], farthest_d[ix])
                        plt.ylabel(ylabel, fontsize='small')
                ix += 1
        plt.tight_layout(pad=0.2)

    def plot2D(self, X, y, split='train', plot_n_samples=200,
               cluster_limits=False, arrows=False,
               method='pca', **method_kwargs):
        model = self.model
        X_train, y_train, X_test, y_test = self.get_data(X, y)

        if split=='test':
            X_train, y_train = X_test, y_test
        elif split=='both':
            X_train = np.concatenate((X_train, X_test), axis=0)
            y_train = np.concatenate((y_train, y_test), axis=0)

        X_norm = self.normalize.transform(X_train)
        y_pred = model.best_estimator_.predict(X_train)
        n_proto = self.cluster.n_clusters
        distances = self.cluster.distances(X_norm)
        cluster_idx = self.cluster.predict(X_norm)
        # projection
        clusters = self.protos
        if method == 'tsne':
            proj = TSNE(n_components=2, **method_kwargs)
            data_clusters = np.concatenate([X_norm, clusters])
            data_clusters = proj.fit_transform(data_clusters)
            data = data_clusters[:X_norm.shape[0],...]
            clusters = data_clusters[X_norm.shape[0]:,...]
            plot2D(data, y_train, y_pred,
                   N=self.N, plot_n_samples=plot_n_samples)
        else:
            pca = plot2D(X_norm , y_train, y_pred,
                         N=self.N, **method_kwargs)
            data = pca.transform(X_norm)
            clusters = pca.transform(clusters)

        for c in range(n_proto):
            marker = '.'
            candidates = np.where(cluster_idx==c)[0]  # returns a tuple
            if candidates.size > 0:
                marker = 'd'
                farthest_idx = candidates[np.argmax(distances[candidates, c], axis=0)]
                X_farthest = data[farthest_idx,:]
                closest_idx = candidates[np.argmin(distances[candidates, c], axis=0)]
                X_closest = data[closest_idx,:]
                def radius(X):
                    r = np.linalg.norm(clusters[c,:]-X)
                    circle(clusters[c,0],clusters[c,1],r)
                def arrow(X,prefix):
                    if cluster_limits:
                        s = prefix+"%d" % (c+1)
                    else:
                        s = ' '
                    plt.annotate(s=s,# xy=X)
                                 xy=(clusters[c,:]),xytext=X,
                                 arrowprops=dict(arrowstyle='<-'),
                                 fontsize='xx-small')
                if candidates.size >= 2 and arrows:
                    arrow(X_farthest,'f')
                    arrow(X_closest,'n')
                elif arrows:
                    arrow(X_closest,'u')


            plt.plot(clusters[c,0], clusters[c,1], ms=5,
                     marker=marker, linewidth=0, color='k')

            proba = self.cluster_proba[c,:]
            top = np.argsort(proba)[-2:][::-1]
            top_p = proba[top]
            s = " p%d"
            if not arrows:
                s += '\n '
            if top_p[0] < 1.0 and top_p[1] >= 0.01:
                s += ','.join(["c%d:%0.2f" % (ix, p) for ix, p in zip(top+1, top_p)])
            elif top_p[0] < 1.0:
                s += "c%d:%0.2f" % (top[0]+1, top_p[0])
            else:
                s += 'c%d' % (top[0]+1)
            plt.annotate(xy=(clusters[c,:]),
                         s=s%(c+1), fontsize='medium')
        plt.axis("off")